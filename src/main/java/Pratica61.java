
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        
        time1.addJogador("Oposto", new Jogador(6,"Rahimova"));
        time1.addJogador("Central", new Jogador(3,"Carol Albuquerque"));
        time1.addJogador("Ponta", new Jogador(10,"Pati"));
        
        time2.addJogador("Oposto", new Jogador(16,"Boskovic"));
        time2.addJogador("Central", new Jogador(18,"Thaísa"));
        time2.addJogador("Ponta", new Jogador(5,"Ting Zhu"));
        
        System.out.println("Posição\tTime1\tTime2");
        Set<String> pos = time1.getJogadores().keySet();
        
        for(String posicao: pos){
            System.out.println(posicao + ": " + time1.getJogadores().get(posicao) +
                               "  " + time2.getJogadores().get(posicao));
        }      
    
    }
}
